@php
  $formatos = Search::formatoBucle($post);
  $organismos = Search::organismoBucle($post);
  $categorias = Search::categoriaBucle($post);
@endphp
 
<article class="informes">
    <h3 class="title__informes"><a href="{{ get_permalink() }}">{{get_the_title()}}</a></h3>  
    @if($organismos)
      @foreach($organismos as $org)  
          <a href="{{get_term_link($org->term_id)}}" class="btn__informes {{ get_field('clases', $org) }}">{{ $org->name }}</a>
      @endforeach
    @endif
    <p class="text__informes">{!! get_the_excerpt() !!}</p>  
     @if($formatos)
      @foreach($formatos as $format)  
          <a href="{{get_term_link($format->term_id)}}" class="btn__informes {{ get_field('clases', $format) }} "> {{ $format->name }} </a>
      @endforeach
    @endif 

     @if($categorias)
      @foreach($categorias as $cat)  
          <a href="{{get_term_link($cat->term_id)}}" class="btn__informes btn__categoria {{ get_field('clases', $cat) }}">{{ $cat->name }}</a>   
      @endforeach
    @endif  
</article>    