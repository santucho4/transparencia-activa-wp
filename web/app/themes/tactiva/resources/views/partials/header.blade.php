<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-12 header__logo{{--  d-flex justify-content-start align-items-center --}}">
                <a href="{{ home_url('/') }}"><img src="@asset('images/logo.svg')" alt="Transparencia Activa" width="252" height="64"></a>
            </div>
            <div class="col-lg-6 col-12 header__texto {{-- d-flex justify-content-end align-items-center --}}">
                <div class="titulo__portal"><a href="{{ home_url('/') }}">{{ $site_name }}</a></div>
            </div>
        </div>
    </div>
</header>