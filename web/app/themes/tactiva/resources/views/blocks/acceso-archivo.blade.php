 {{--
	Title: Acceso Archivo
	Description: Acceso archivo 
	Category: formatting
	Icon: admin-comments
	Keywords: hero
	Mode: edit
	Align: left
	PostTypes: page post
	SupportsAlign: left right
	SupportsMode: false
	SupportsMultiple: false
--}}

<!-- Ley 104 -->
<section class="ley-104" data-{{ $block['id'] }} class="{{ $block['classes'] }}">
    <div class="container">
        <div class="row row d-flex justify-content-center">
            <div class="col-lg-8 col-12">
            	<p class="ley-104__texto"><strong>{{ get_field('titulo') }}</strong></p>
                <p class="ley-104__texto">{!! get_field('descripcion') !!}</p>
            </div>
            <div class="col-lg-2 col-12">
                <a href="{{ get_field('cta-href') }}" class="btn btn-primary btn-descargar">{{ get_field('cta-text') }}</a>
            </div>
            <div class="col-lg-2 col-12"> 
                <a href="{{ get_field('cta-href-2') }}" class="btn btn-primary btn-ver" target="_blank">{{ get_field('cta-text-2') }}</a> 
            </div>
        </div>
    </div>
</section>   