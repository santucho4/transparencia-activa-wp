<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'a8dee00ea2b86cbd10826c540489d762aa6dea98',
    'name' => 'roots/bedrock',
  ),
  'versions' => 
  array (
    'composer/installers' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1a0357fccad9d1cc1ea0c9a05b8847fbccccb78d',
    ),
    'graham-campbell/result-type' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e279d2cd5d7fbb156ce46daada972355cea27bb',
    ),
    'johnpbloch/wordpress' => 
    array (
      'pretty_version' => '5.6.1',
      'version' => '5.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd7a597988102967cdfc28851b6b897d018613823',
    ),
    'johnpbloch/wordpress-core' => 
    array (
      'pretty_version' => '5.6.1',
      'version' => '5.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '82592ec73d42cf784da38adb0028a24dbacab1b4',
    ),
    'johnpbloch/wordpress-core-installer' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'mwdelaney/sage-acf-gutenberg-blocks' => 
    array (
      'pretty_version' => '0.7',
      'version' => '0.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '795527e63b45c8426741a074270355953b28ce37',
    ),
    'oscarotero/env' => 
    array (
      'pretty_version' => 'v2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0da22cadc6924155fa9bbea2cdda2e84ab7cbdd3',
    ),
    'phpoption/phpoption' => 
    array (
      'pretty_version' => '1.7.5',
      'version' => '1.7.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '994ecccd8f3283ecf5ac33254543eb0ac946d525',
    ),
    'roave/security-advisories' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '640ff0b5dcacc0958534c8c0255b90697f3eb2a8',
    ),
    'roots/bedrock' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'a8dee00ea2b86cbd10826c540489d762aa6dea98',
    ),
    'roots/bedrock-autoloader' => 
    array (
      'pretty_version' => '1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f508348a3365ab5ce7e045f5fd4ee9f0a30dd70f',
    ),
    'roots/wordpress' => 
    array (
      'pretty_version' => '5.5.3',
      'version' => '5.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5.5.3',
    ),
    'roots/wordpress-core-installer' => 
    array (
      'pretty_version' => '1.100.0',
      'version' => '1.100.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '73f8488e5178c5d54234b919f823a9095e2b1847',
    ),
    'roots/wp-config' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '37c38230796119fb487fa03346ab0706ce6d4962',
    ),
    'roots/wp-password-bcrypt' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5cecd2e98ccc3193443cc5c5db9b3bc7abed5ffa',
    ),
    'roots/wp-stage-switcher' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c8bffa5a5d757437112f32833fb9e28dcbfecafe',
    ),
    'roundcube/plugin-installer' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'shama/baton' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'squizlabs/php_codesniffer' => 
    array (
      'pretty_version' => '3.5.8',
      'version' => '3.5.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d583721a7157ee997f235f327de038e7ea6dac4',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6c942b1ac76c82448322025e084cadc56048b4e',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5232de97ee3b75b0360528dae24e73db49566ab1',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc3063ba22c2a1fd2f45ed856374d79114998f91',
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'v5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b3eac5c7ac896e52deab4a99068e3f4ab12d9e56',
    ),
    'wordpress/core-implementation' => 
    array (
      'provided' => 
      array (
        0 => '5.6.1',
      ),
    ),
    'wpackagist-plugin/autodescription' => 
    array (
      'pretty_version' => '4.1.3',
      'version' => '4.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'tags/4.1.3',
    ),
    'wpackagist-plugin/breadcrumb-navxt' => 
    array (
      'pretty_version' => '6.6.0',
      'version' => '6.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'tags/6.6.0',
    ),
    'wpackagist-plugin/duplicate-post' => 
    array (
      'pretty_version' => '4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'tags/4.1.1',
    ),
    'wpackagist-plugin/safe-redirect-manager' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'tags/1.10.0',
    ),
    'wpackagist-plugin/simple-page-ordering' => 
    array (
      'pretty_version' => '2.3.4',
      'version' => '2.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'tags/2.3.4',
    ),
  ),
);
